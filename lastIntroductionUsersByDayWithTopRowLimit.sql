SELECT 	to_char(to_timestamp(max(histTb.introductiondate)), 'DD-MM-YYYY') as introductionDate,
	count(histTb.idrow) as usersCount
FROM public.vkusersgrouphistory histTb
LEFT OUTER JOIN public.vkuser userTb
ON histTb.iduser = userTb.iduser
WHERE histTb.idgroup = 1959 AND histTb.releasedate < 0
GROUP BY extract(YEAR FROM to_timestamp(histTb.introductiondate)),extract(MONTH FROM to_timestamp(histTb.introductiondate)),extract(DAY FROM to_timestamp(histTb.introductiondate))
ORDER BY extract(YEAR FROM to_timestamp(histTb.introductiondate)) DESC,extract(MONTH FROM to_timestamp(histTb.introductiondate)) DESC,extract(DAY FROM to_timestamp(histTb.introductiondate)) DESC
LIMIT 30