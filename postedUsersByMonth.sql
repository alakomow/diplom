SELECT  usersByPosts.userName as userName,
	count(usersByPosts.messageID) as postsCount,
	max (userID) as tmpID,
	min (userID) as tmpID,
	extract(MONTH FROM to_timestamp(usersByPosts.postDate)) as postDateMonth,
	extract(YEAR FROM to_timestamp(usersByPosts.postDate)) as postDateYear,
	to_char(to_timestamp(max(usersByPosts.postDate)), 'MM-YYYY') as messageDate
FROM
	(SELECT msgTb.idgroup as userID,
	       msgTb.createdate as postDate,
	       grTb.groupname as userName,
	       msgTb.idmessage as messageID
	FROM public.vkmessage msgTb
	LEFT OUTER JOIN public.vkgroup grTb
	ON (msgTb.idgroup = grTb.idgroup) OR (msgTb.iduser = grTb.idgroup) OR (-msgTb.iduser = grTb.idgroup)
	WHERE 	msgTb.idgroup = 1959 AND
		(msgTb.iduser = msgTb.idgroup OR -msgTb.iduser = msgTb.idgroup) AND
		extract(MONTH FROM to_timestamp(msgTb.createdate)) = extract(MONTH FROM clock_timestamp()) AND
		extract(YEAR FROM to_timestamp(msgTb.createdate)) = extract(YEAR FROM clock_timestamp())
 
	UNION ALL

	SELECT msgTb.idgroup as userID,
	       msgTb.createdate as postDate,
	       userTb.firstname || userTb.lastName as userName,
	       msgTb.idmessage as messageID
	FROM public.vkmessage msgTb
	LEFT OUTER JOIN public.vkuser userTb
	ON msgTb.iduser = userTb.iduser AND (msgTb.idgroup != userTb.iduser OR -msgTb.idgroup != userTb.iduser)
	WHERE 	msgTb.idgroup = 1959 AND
		(msgTb.iduser != msgTb.idgroup AND -msgTb.iduser != msgTb.idgroup) --AND
		extract(MONTH FROM to_timestamp(msgTb.createdate)) = extract(MONTH FROM clock_timestamp()) AND
		extract(YEAR FROM to_timestamp(msgTb.createdate)) = extract(YEAR FROM clock_timestamp())
	) as usersByPosts
GROUP BY usersByPosts.userID,usersByPosts.userName,postDateMonth,postDateYear
ORDER BY postDateYear DESC, postDateMonth DESC;
	


--  DROP FUNCTION postedUsersByMonth();
-- CREATE OR REPLACE FUNCTION postedUsersByMonth() RETURNS TABLE (
--    userName   character varying(255),
--    postsCount bigint,
--    postDateMonth   double precision
--   ,postDateYear double precision
--   ) LANGUAGE plpgsql AS $$
-- DECLARE
--   currentGroupID BIGINT := 1959;
-- BEGIN
--   RETURN QUERY 
-- SELECT  usersByPosts.userName as userName,
-- 	count(usersByPosts.messageID) as postsCount,
-- 	extract(MONTH FROM to_timestamp(usersByPosts.postDate)) as postDateMonth,
-- 	extract(YEAR FROM to_timestamp(usersByPosts.postDate)) as postDateYear
-- FROM
-- 	(SELECT msgTb.idgroup as userID,
-- 	       msgTb.createdate as postDate,
-- 	       grTb.groupname as userName,
-- 	       msgTb.idmessage as messageID
-- 	FROM public.vkmessage msgTb
-- 	LEFT OUTER JOIN public.vkgroup grTb
-- 	ON msgTb.idgroup = grTb.idgroup
-- 	WHERE 	msgTb.idgroup = currentGroupID() AND
-- 		to_timestamp(createdate) >= (clock_timestamp() - INTERVAL '1 YEAR')

-- 	UNION ALL

-- 	SELECT 1 as userID,
-- 	       2 as postDate,
-- 	       'fName' ||' '|| 'lName' as userName,
-- 	       3 as messageID
-- 	) as usersByPosts
-- GROUP BY usersByPosts.userID,usersByPosts.userName,postDateMonth,postDateYear
-- ORDER BY postDateYear DESC, postDateMonth DESC;
-- END
-- $$;
-- select * from postedUsersByMonth();