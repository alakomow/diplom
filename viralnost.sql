with pospsForPeriod as (
	SELECT public.vkmessage.idmessage as messageID,
		public.vkmessage.idgroup as groupID,
		public.vkmessage.createdate as postDate,
		public.vkmessage.commentscnt+ public.vkmessage.likescnt+public.vkmessage.repostscnt as actionCnt
	FROM public.vkmessage
	WHERE public.vkmessage.idgroup = 1959 --AND 
	--to_timestamp(public.vkmessage.createdate) >=  clock_timestamp() - INTERVAL '1 WEEK + 1 DAY'
	ORDER BY public.vkmessage.createdate DESC
	LIMIT 40
),
	usersCount as (
	SELECT count(*) as cnt FROM public.vkusersgrouphistory histTb
	where histTb.idgroup = 1959 and histTb.releasedate < 0
)


SELECT viralnostData.messageID as messageID,
	to_char(to_timestamp(viralnostData.postDate), 'DD-MM-YYYY') as postDate,
 round(cast((cast(viralnostData.actionCnt as double precision) /
		cast(viralnostData.allUserCount as double precision)) *100 as numeric),2) as viralost
	
FROM
	(SELECT pospsForPeriod.messageID as messageID,
		pospsForPeriod.actionCnt as actionCnt,
		pospsForPeriod.postDate as postDate,
		(select usersCount.cnt from usersCount) as allUserCount
	FROM pospsForPeriod) as viralnostData

	



