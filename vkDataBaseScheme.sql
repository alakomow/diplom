-- Table: public.vkgroup

-- DROP TABLE public.vkgroup;

CREATE TABLE public.vkgroup
(
  idgroup bigint,
  groupname character varying(255),
  groupdomain character varying(255),
  lastupdate double precision
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.vkgroup
  OWNER TO diplom;


-- Table: public.vkmessage

-- DROP TABLE public.vkmessage;

CREATE TABLE public.vkmessage
(
  idmessage bigint,
  createdate double precision,
  commentscnt bigint,
  likescnt bigint,
  repostscnt bigint,
  iduser bigint,
  idgroup bigint,
  text text
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.vkmessage
  OWNER TO diplom;

  
-- Table: public.vkuser

-- DROP TABLE public.vkuser;

CREATE TABLE public.vkuser
(
  iduser bigint NOT NULL,
  firstname character varying(255),
  lastname character varying(255)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.vkuser
  OWNER TO diplom;

-- Table: public.vkusersgrouphistory

-- DROP TABLE public.vkusersgrouphistory;

CREATE TABLE public.vkusersgrouphistory
(
  idrow bigint NOT NULL,
  iduser bigint,
  idgroup bigint,
  introductiondate double precision,
  releasedate double precision,
  CONSTRAINT "idRowPkKey" PRIMARY KEY (idrow)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.vkusersgrouphistory
  OWNER TO diplom;

