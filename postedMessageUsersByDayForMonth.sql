SELECT count(msgTb.iduser) as usersCount,
	to_char(to_timestamp(max(msgTb.createdate)), 'DD-MM-YYYY') as postDate
FROM
(SELECT  max(msgTb.idmessage) as messageID
	FROM public.vkmessage msgTb
	WHERE 	msgTb.idgroup = 1959 AND
		to_timestamp(msgTb.createdate) >=  clock_timestamp() - INTERVAL '1 MONTH + 1 DAY'
	GROUP BY extract(YEAR FROM to_timestamp(msgTb.createdate)),
		extract(MONTH FROM to_timestamp(msgTb.createdate)),
		extract(DAY FROM to_timestamp(msgTb.createdate)),
		msgTb.iduser
) as messages
INNER JOIN public.vkmessage msgTb
ON messages.messageID = msgTb.idmessage
GROUP BY extract(YEAR FROM to_timestamp(msgTb.createdate)),
	extract(MONTH FROM to_timestamp(msgTb.createdate)),
	extract(DAY FROM to_timestamp(msgTb.createdate)),
	msgTb.idmessage
ORDER BY extract(YEAR FROM to_timestamp(msgTb.createdate)) DESC,
	extract(MONTH FROM to_timestamp(msgTb.createdate)) DESC,
	extract(DAY FROM to_timestamp(msgTb.createdate)) DESC

		