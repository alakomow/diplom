#!/usr/bin/env python3.5 -B

import VKAPI

class Comments():
	ID = None
	userID = None
	@staticmethod
	def createFromData(data):
		if data.get('id',None) is not None:
			comment = Comments()
			comment.ID = data.get('id')
			comment.userID = data.get('from_id')
			return comment

		return None

class PostsAciveUser():
	ID = None
	userName = None
	actionsCount = 0

class PostManager():
	
	def getCommentedUsers(self, owner_id = None, post_id = None, activeUsers = []):
		if owner_id is None:
			return activeUsers

		comments = VKAPI.getComments(owner_id = owner_id, post_id = post_id)

		for  x in comments.get('items'):
			
			comment = Comments.createFromData(x)
			if comment is not None and comment.userID is not None:

				user = None
				for auser in activeUsers:
					if auser.ID == comment.userID:
						user = auser
						break
				if user is None:
					dataArray = None
					if comment.userID < 0:
						dataArray = comments.get('groups')
					else:
						dataArray = comments.get('profiles')

					for dictObj in dataArray:
						if dictObj.get('id') == comment.userID:
							# Found user
							user = PostsAciveUser()
							user.ID = comment.userID
							user.userName = dictObj.get('first_name','') + " " + dictObj.get('last_name','')
							activeUsers.append(user)
							break
						if dictObj.get('id') == -comment.userID:
							#Found froup
							user = PostsAciveUser()
							user.ID = comment.userID
							user.userName = dictObj.get('name','')
							activeUsers.append(user)
							break
				if user is not None:
					user.actionsCount +=1
		activeUsers.sort(key=lambda x: x.actionsCount, reverse=True)
		return activeUsers

	def getReposteredUsers(self, owner_id = None, post_id = None, activeUsers = []):
		if owner_id is None:
			return activeUsers

		comments = VKAPI.getReposts(owner_id = owner_id, post_id = post_id)

		for  x in comments.get('items'):
			
			comment = Comments.createFromData(x)
			if comment is not None and comment.userID is not None:

				user = None

				for auser in activeUsers:
					if auser.ID == comment.userID:
						user = auser
						break
				if user is None:
					dataArray = None
					if comment.userID < 0:
						dataArray = comments.get('groups')
					else:
						dataArray = comments.get('profiles')

					for dictObj in dataArray:
						if dictObj.get('id') == comment.userID:
							# Found user
							user = PostsAciveUser()
							user.ID = comment.userID
							user.userName = dictObj.get('first_name','') + " " + dictObj.get('last_name','')
							activeUsers.append(user)
							break
						if dictObj.get('id') == -comment.userID:
							#Found froup
							user = PostsAciveUser()
							user.ID = comment.userID
							user.userName = dictObj.get('name','')
							activeUsers.append(user)
							break
				if user is not None:
					user.actionsCount +=1
		activeUsers.sort(key=lambda x: x.actionsCount, reverse=True)
		return activeUsers

	def getLikedUsers(self, owner_id = None, post_id = None, activeUsers = []):
		if owner_id is None:
			return activeUsers

		likes = VKAPI.getLikes(owner_id = owner_id, post_id = post_id)


		for like in likes.get('items'):
			userID = like.get('id')

			user = None
			for auser in activeUsers:
				if auser.ID == userID:
					user = auser
					break
			if user is None:
				user = PostsAciveUser()
				if like.get('type') == "profile":
					user.ID = userID
					user.userName = like.get('first_name','') + " " + like.get('last_name','')
				else:
					user.ID = -userID
					user.userName = like.get('name') 
				activeUsers.append(user)
			if user is not None:
				user.actionsCount +=1
		activeUsers.sort(key=lambda x: x.actionsCount, reverse=True)
		return activeUsers





