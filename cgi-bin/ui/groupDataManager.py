#!/usr/bin/env python3.5 -B

import vk
import datetime
import time
from datetime import date
from time import mktime
import operator
from vk.exceptions import VkAuthError, VkAPIError


import postDataManager


class GRoupData(object):
	groupID = None
	groupName = None
	groupMembersCount = None
	postsStatistic = None


class GRoupPostStatisticsData(object):
	dayLimit = 1
	avgPostsPerDay = 0
	
	activeUsers = []
	# Users activity (Likes + Comments + Reposts)
	activeGroupUsers = []
	posts = []
	providerTop = []
	posterTop = []

class GRoupUser(object):
	userID = None
	userPostsCount = 0
	userName = ""
	userProvideredMessageCount = 0


class  GRoupPost(object):
	postID = None
	text = None
	groupID = None
	poster = None
	provider = None
	providers = []
	posters = []
	date = None

def vkAPI():
	return vk.API(vk.Session(),v='5.52')

def loadInfoGroup(group_id):
	responce = vkAPI().groups.getById(group_id = group_id,fields="members_count")
	return responce[0]

def loadPostStatistic(groupID,dayLimit = 1):
	postsStatistics = GRoupPostStatisticsData()
	postsStatistics.dayLimit = dayLimit
	messagesPerday = list(range(dayLimit))
	for i in range(dayLimit):
	   messagesPerday[i] = 0

	postedUserIDs = {}


	stepCount = 100
	lastOffset = 0
	count = 1

	acrivePostsUsers = []

	while lastOffset < count:
		# print ("Request group posts for group ="+str(groupID)+" countFor request="+str(stepCount)+" offset="+str(lastOffset)+" all users count = "+str(count))
		try:
			groupWall = vkAPI().wall.get(owner_id=-groupID,count=stepCount,offset = lastOffset)

		except VkAPIError as e:
			print("Load posts error :"+str(e))
			exit(-2)

		else:
			pass
		finally:
			pass

		messages = groupWall.get('items')
		count = groupWall.get('count')
		lastOffset = lastOffset + stepCount
		
		if messages is not None:
			timeLastMonth = mktime((datetime.datetime.now() - datetime.timedelta(days=dayLimit)).timetuple())
			for i in range(1,len(messages)):
				if messages[i].get('date') >= timeLastMonth:
					dayIndex = int(round((messages[i].get('date') - timeLastMonth)/(60 * 60 * 24),0)) - 1
					posterID =  messages[i].get('from_id')

					# print ('Пост:' + str(posterID))
					# if posterID < 0 :
						# posterID = -posterID
					
					postedUserIDs[posterID] = postedUserIDs.get(posterID,0) + 1
					# print (dayIndex)
					messagesPerday[dayIndex] = messagesPerday[dayIndex] + 1
					# print (str(messagesCount))

					post = GRoupPost()
					post.postID = messages[i].get('id')
					post.groupID = messages[i].get('owner_id')
					post.text = messages[i].get('text',None)
					if post.text is not None and len(post.text) < 1:
						post.text = None
					postManager = postDataManager.PostManager()
					acrivePostsUsers = postManager.getCommentedUsers(owner_id = post.groupID,post_id=messages[i].get('id'), activeUsers = acrivePostsUsers)
					acrivePostsUsers = postManager.getReposteredUsers(owner_id = post.groupID,post_id=messages[i].get('id'), activeUsers = acrivePostsUsers)
					acrivePostsUsers = postManager.getLikedUsers(owner_id = post.groupID,post_id=messages[i].get('id'), activeUsers = acrivePostsUsers)
					
					postsStatistics.posts.append(post)
				else:
					lastOffset = count

	postsStatistics.activeGroupUsers = acrivePostsUsers
	avgPerDay = 0
	for i in range(dayLimit):
		avgPerDay = avgPerDay + messagesPerday[i]

	avgPerDay = avgPerDay/dayLimit
	postsStatistics.avgPostsPerDay = avgPerDay

	dc_sort = sorted(postedUserIDs.items(),key = operator.itemgetter(1),reverse = True)
	for x in dc_sort:
		activeUser = GRoupUser()
		activeUser.userID = x[0]
		activeUser.userPostsCount = x[1]
		postsStatistics.activeUsers.append(activeUser)

	mostPopularUserIds = ""
	mostPopularGroupIds = ""
	for x in postsStatistics.activeUsers:
		if x.userID > 0 :
			if mostPopularUserIds == "":
				mostPopularUserIds = str(x.userID)
			else:
				mostPopularUserIds = mostPopularUserIds + ',' + str(x.userID)
		else:
			if mostPopularGroupIds == "":
				mostPopularGroupIds = str(-x.userID)
			else:
				mostPopularGroupIds = mostPopularGroupIds + ',' + str(-x.userID)

	try:
		activeUsers = []
		if len(mostPopularUserIds) > 0:
			activeUsers = vkAPI().users.get(user_ids=mostPopularUserIds)

	except VkAPIError as e:
		print("Load active users error :"+str(e))
	else:
		pass
	finally:
		pass

	try:
		activeGroups = []
		if len(mostPopularGroupIds) > 0:
			activeGroups = vkAPI().groups.getById(group_ids=mostPopularGroupIds)

	except VkAPIError as e:
		print("Load active groups error :"+str(e))
	else:
		pass
	finally:
		pass

	for x in postsStatistics.activeUsers:
		if x.userID < 0:
			for group in activeGroups:
				if group.get('id') == -x.userID:
					x.userName = group.get('name')
					break
		else:
			for user in activeUsers:
				if user.get('id') == x.userID:
					x.userName = user.get('first_name') + " " + user.get('last_name')
					break

	return postsStatistics

def buildProviders(posts:[GRoupPost]):

	startIndex = 0
	step = 100
	maxIndex = len(posts)
	while startIndex < maxIndex:

		idSstring = None
		stopIndex = startIndex + step
		if stopIndex > maxIndex:
			stopIndex = maxIndex
		for i in range(startIndex,stopIndex):
			if idSstring is not None:
				idSstring = idSstring + ','
			else:
				idSstring = ""	
			idSstring = idSstring + str(posts[i].groupID) + '_' + str(posts[i].postID)

		try:
			postsResponce = None
			if idSstring is not None and len(idSstring) > 0:
				postsResponce = vkAPI().wall.getById(posts=idSstring,extended = 1, copy_history_depth = 1,fields = "first_name,last_name")

		except VkAPIError as e:
			print("Load active groups error :"+str(e))
		else:
			pass
		finally:
			pass

		for data in postsResponce.get('items'):
			postIndex = -1
			for i in range(startIndex,stopIndex):
				if posts[i].postID == data.get('id'):
					postIndex = i
					break
			posts[postIndex].date = data.get('date')
			postHistory = data.get('copy_history')

			poster = GRoupUser()
			poster.userID = data.get('from_id')
			if poster.userID < 0:
				for gr in postsResponce.get('groups'):
					if gr.get('id') == -poster.userID:
						poster.userName = gr.get('name')
						break
			else:
				for user in postsResponce.get('profiles'):
					if user.get('id') == poster.userID:
						poster.userName = user.get('first_name','') + " " + user.get('last_name','')
			posts[postIndex].poster = poster

			if postHistory is not None :
				if len(postHistory) > 0 :
					histObj = postHistory[0]
					if histObj is not None:
						provider = GRoupUser()
						provider.userID = histObj.get('from_id')
						if provider.userID < 0:
							for gr in postsResponce.get('groups'):
								if gr.get('id') == -provider.userID:
									provider.userName = gr.get('name')
									break
						else:
							for user in postsResponce.get('profiles'):
								if user.get('id') == provider.userID:
									provider.userName = user.get('first_name') + " " + user.get('last_name')
						posts[postIndex].provider = provider
			#start search provider
			searchText = data.get('text')
			try:
				searchResponce = None
				if searchText is not None and len(searchText) > 0:
					searchResponce = vkAPI().newsfeed.search(q=searchText,extended=1, count=20,start_time =posts[postIndex].date - datetime.timedelta(days=365*36).total_seconds(),end_time = posts[postIndex].date)
			except VkAPIError as e:
				print("Load active groups error :"+str(e))
			else:
				pass
			finally:
				pass


			if searchResponce is not None:
				# print (len(searchResponce.get('items')))
				responceObjects = []
				for searchData in searchResponce.get('items'):
					if searchData.get('date',time.time()) < posts[postIndex].date:
						provider = GRoupUser()
						provider.userID = searchData.get('from_id')
						provider.date = searchData.get('date')
						if provider.userID < 0:
							for gr in searchResponce.get('groups'):
								if gr.get('id') == -provider.userID:
									provider.userName = gr.get('name')
									break
						else:
							for user in searchResponce.get('profiles'):
								if user.get('id') == provider.userID:
									provider.userName = user.get('first_name') + " " + user.get('last_name')
									break
						responceObjects.append(provider)
				posts[postIndex].providers = responceObjects
				# Posters------
			try:
				searchResponce = None
				if searchText is not None and len(searchText) > 0:
					searchResponce = vkAPI().newsfeed.search(q=searchText,extended=1, count=20,start_time =posts[postIndex].date,end_time =time.time())
			except VkAPIError as e:
				print("Load active groups error :"+str(e))
			else:
				pass
			finally:
				pass


			if searchResponce is not None:
				# print (len(searchResponce.get('items')))
				responceObjects = []
				for searchData in searchResponce.get('items'):
					if searchData.get('date',time.time()) > posts[postIndex].date:
						provider = GRoupUser()
						provider.userID = searchData.get('from_id')
						provider.date = searchData.get('date')
						if provider.userID < 0:
							for gr in searchResponce.get('groups'):
								if gr.get('id') == -provider.userID:
									provider.userName = gr.get('name')
									break
						else:
							for user in searchResponce.get('profiles'):
								if user.get('id') == provider.userID:
									provider.userName = user.get('first_name') + " " + user.get('last_name')
									break
						responceObjects.append(provider)
				posts[postIndex].posters = responceObjects

		startIndex = stopIndex
	# for x in posts:
	# 	print ("ProvidersCount= " + str(len(x.providers)))
	return posts

def buildProviderTop(postStatictic:GRoupPostStatisticsData):
	topIsersIds = {}
	userNames = {}
	for post in postStatictic.posts:
		for provider in post.providers:
			topIsersIds[provider.userID] = topIsersIds.get(provider.userID,0) + 1
			userNames[provider.userID] = provider.userName
	
	dc_sort = sorted(topIsersIds.items(),key = operator.itemgetter(1),reverse = True)
	maxValue = 0
	for x in dc_sort:
		if x[1] < maxValue and maxValue == 0:
			maxValue = x[1]

		if maxValue/2 > x[1]:
			return
		activeUser = GRoupUser()
		activeUser.userID = x[0]
		activeUser.userProvideredMessageCount = x[1]
		activeUser.userName = userNames.get(x[0],None)
		postStatictic.providerTop.append(activeUser)

def buildPostersTop(postStatictic:GRoupPostStatisticsData):
	topIsersIds = {}
	userNames = {}
	for post in postStatictic.posts:
		for poster in post.posters:
			topIsersIds[poster.userID] = topIsersIds.get(poster.userID,0) + 1
			userNames[poster.userID] = poster.userName
	
	dc_sort = sorted(topIsersIds.items(),key = operator.itemgetter(1),reverse = True)
	maxValue = 0
	for x in dc_sort:
		if x[1] < maxValue and maxValue == 0:
			maxValue = x[1]

		if maxValue/2 > x[1]:
			return
		activeUser = GRoupUser()
		activeUser.userID = x[0]
		activeUser.userProvideredMessageCount = x[1]
		activeUser.userName = userNames.get(x[0],None)
		postStatictic.posterTop.append(activeUser)
	



def groupObject(group_id, dayLimit):
	returnObject = GRoupData()
	if group_id is None:
		return returnObject

	groupResponceObject = loadInfoGroup(group_id)

	returnObject.groupID = groupResponceObject.get("id")
	returnObject.groupName =  groupResponceObject.get("name")
	returnObject.groupMembersCount = groupResponceObject.get("members_count")

	returnObject.postsStatistic = loadPostStatistic(groupID=returnObject.groupID,dayLimit = dayLimit)

	returnObject.postsStatistic.posts = buildProviders(returnObject.postsStatistic.posts)

	buildProviderTop(returnObject.postsStatistic)
	buildPostersTop(returnObject.postsStatistic)

	return returnObject
