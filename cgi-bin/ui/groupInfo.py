#!/usr/bin/env python3.5 -B
print('Content-type: text/html \n')
import cgi
import groupDataManager
import sys

def linkToVK(idUSer, nameUser = None):
	link = "https://vk.com/"
	if idUSer < 0:
		link = link + "club" + str(-idUSer)
	else:
		link = link + "id" + str(idUSer)
	linkTitle = idUSer
	if nameUser is not None:
		linkTitle = nameUser
	return "<a href=\""+link+"\" target=\"_blank\">"+ str(linkTitle)+"</a>"

def linkToHost(idUser):
	if idUser < 0:
		link = "./groupInfo.py?GROUPID="+str(-idUser)
		return "<a href=\""+link+"\" target=\"_blank\">"+ str(idUser)+"</a>"
	return str(idUser)

kGROUPIDGETPARAMKEY =  "GROUPID"
kDAYCOUNTGETPARAMKEY = "DAYCOUNT"

form = cgi.FieldStorage()
groupID = form.getfirst(kGROUPIDGETPARAMKEY, None)
dayLimit = int(form.getfirst(kDAYCOUNTGETPARAMKEY,2))



if groupID is None:
	groupID = 'super_cutting'

print("<!DOCTYPE HTML>\
<html>\
<head>\
<meta charset=\"utf-8\">\
<title>Информация по группе</title>\
</head>\
<body>\
    <form action=\"/cgi-bin/ui/groupInfo.py\">\
        <label type=\"text\">Введите id группы</label>\
        <input type=\"text\" name=\""+ kGROUPIDGETPARAMKEY +"\" value=\""+ str(groupID) +"\">\
        <label type=\"text\">Введите колличество дней для анализа</label>\
        <input type=\"text\" name=\""+ kDAYCOUNTGETPARAMKEY +"\" value=\""+ str(dayLimit) +"\">\
        <input type=\"submit\">\
    </form>")




groupData = groupDataManager.groupObject(groupID, dayLimit)

if groupData.groupID is None:
	print ("<h2>ID группы - не задано</H2>")
	print ("Продолжение работы не возможно")
else:
	print ("<h2>"+ linkToHost(-groupData.groupID) +" " +linkToVK(-groupData.groupID,groupData.groupName) + '</h2>')

if groupData.groupMembersCount is None:
	groupData.groupMembersCount = 0

print('<table border="1">')
print ('<tr>')
print ('<th>Колличество пользователей</th>')
print ('<td>'+ str(groupData.groupMembersCount) +'</td>')
print ('</tr>')
if groupData.postsStatistic is not None:
	print('<tr>')
	print('<th>Колличество пользователей публиковавших за последние '+ str(groupData.postsStatistic.dayLimit) +' дней</th>')
	print ('<td>'+ str(len(groupData.postsStatistic.activeUsers)) +'</td>')
	print('</tr>')
	print('<tr>')
	print('<th>Колличество сообщений за последние '+ str(groupData.postsStatistic.dayLimit) +' дней</th>')
	print ('<td>'+ str(len(groupData.postsStatistic.posts)) +'</td>')
	
	uniquePosts = []
	textPosts = []
	for x in groupData.postsStatistic.posts:
		if x.text is not None:
			textPosts.append(x)
		if x.provider is None and (x.providers is None or len(x.providers) == 0) and x.provider is None and x.text is not None:	
			uniquePosts.append(x)
	print ('</tr><tr>')
	print ('<th>Колличество текстовых сообщений за последние '+ str(groupData.postsStatistic.dayLimit) +' дней</th>')
	print ('<td>'+ str(len(textPosts)) +'</td>')
	print ('</tr><tr>')
	print ('<th>Колличество уникальных текстовых сообщений за последние '+ str(groupData.postsStatistic.dayLimit) +' дней</th>')
	print ('<td>'+ str(len(uniquePosts)) +'  ('+ str(round((len(uniquePosts)/len(textPosts))*100,0)) +'%)</td>')
	print ('</tr><tr>')
	print('<th>Среднее колличество(всех) сообщений в день</th>')
	print ('<td>'+ str(round(groupData.postsStatistic.avgPostsPerDay,3)) +'</td>')
	print ('</tr>')
	print ('</table>')


	activitesCount = 0
	for x in groupData.postsStatistic.activeGroupUsers:
		activitesCount += x.actionsCount

	print('<table border="1">')
	print('<tr>')
	print('<th>Колличество действий пользователей(репосты+комментарии+лайки) за последние '+ str(groupData.postsStatistic.dayLimit) +' дней</th>')
	print('<td>'+ str(activitesCount) +'</td>')
	print('</tr><tr>')
	print('<th>Среднее колличество действий пользователей(репосты+комментарии+лайки) в день за последние '+ str(groupData.postsStatistic.dayLimit) +' дней</th>')
	print('<td>'+ str(round(activitesCount/groupData.postsStatistic.dayLimit,0)) +'</td>')
	print('</tr>')
	print('</table>')

	print ("<h4>Активные пользователи</h4><br>")

	print('<table border="1">')
	print('<tr><th>По постам</th><th>По действиям</th></tr>')
	print('<tr>')
	print('<td>')
	print ('<table border="1">')
	print ('<tr>')
	print('<th>ID пользователя</th>')
	print('<th>Имя пользователя</th>')
	print('<th>Колличество постов</th>')
	print('</tr>')
	activeUserPrintCount = 0
	for activeUser in groupData.postsStatistic.activeUsers:
		if activeUserPrintCount > 10:
			break
		print ('<tr>')
		print ('<td>'+ linkToHost(activeUser.userID) +'</td>')
		print ('<td>'+ linkToVK(activeUser.userID,activeUser.userName) +'</td>')
		print ('<td>'+ str(activeUser.userPostsCount)  +'</td>')
		print ('</tr>')
		activeUserPrintCount +=1
		# print("Пользователь: <<" + activeUser.userName + ">>"+ " сделал(а) " + str(activeUser.userPostsCount) + ' постов(а)<br>')
	print('</table><br>')
	print('</td><td>')
	print('<table border="1">')
	print('<tr><th>ID пользователя</th><th>Имя пользователя</th><th>Колличество действий</th></tr>')
	activeUserPrintCount = 0
	for activeUser in groupData.postsStatistic.activeGroupUsers:
		if activeUserPrintCount > 10:
			break
		print ('<tr>')
		print ('<td>'+ linkToHost(activeUser.ID) +'</td>')
		print ('<td>'+ linkToVK(activeUser.ID,activeUser.userName) +'</td>')
		print ('<td>'+ str(activeUser.actionsCount)  +'</td>')
		print ('</tr>')
		activeUserPrintCount +=1
	print('</table>') 
	print('</td>')
	print('</tr>')
	print('</table>')

	



	# print ('<h3>Уникальность постов</h3>')
	# for post in groupData.postsStatistic.posts:
	# 	providerName = "!Не найден!"
	# 	if post.provider is not None:
	# 		providerName = post.provider.userName

	# 	posterName = "!!Не определено!!"
	# 	if post.poster is not None:
	# 		posterName = post.poster.userName

	# 	print ('Пост ID: '+ str(post.postID)+ " Запостил(а) \'"+posterName+"\", поставщик поста(по репосту): \""+ providerName +"\" <br>")

	# 	userIds = ""
	# 	for x in post.providers:
	# 		if userIds == "":
	# 			userIds =userIds +"("+ str(x.userID) +")"
	# 		else:
	# 			userIds = userIds +"," +"("+ str(x.userID) +")"
	# 	print ("*************" + str(len(post.providers)))
	# 	print ('-------------Пост ID: '+ str(post.postID)+ " Запостил(а) \'"+posterName+"\", поставщики поста: \""+ userIds +"\" <br>")


	print ('<table border="1">')
	print ('<tr>')
	print ('<th> Топ поставщиков контента </th>')
	print ('<th> Топ потребителей контента </th>')
	print ('</tr>')
	print ('<tr>')
	print ('<td valign="top">')
	print ('<table border="1"><tr><th>ID пользователя</th><th>Имя пользователя</th><th>Колличество сообщений</th></tr>')
	minMessageCount = 0
	if groupData.postsStatistic.providerTop is not None:
		if len(groupData.postsStatistic.providerTop) > 0:
			pr = groupData.postsStatistic.providerTop[0]
			minMessageCount = (pr.userProvideredMessageCount/2) - 1
	for provider in groupData.postsStatistic.providerTop:
		if provider.userProvideredMessageCount < minMessageCount:
			break
		print ('<tr>')
		print ('<td>'+ linkToHost(provider.userID) +'</td>')
		print ('<td>'+ linkToVK(provider.userID,provider.userName) +'</td>')
		print ('<td>'+ str(provider.userProvideredMessageCount) +'</td>')
		print ('</tr>')
	print ('</table>')
	print ('</td><td valign="top">')
	print ('<table border="1"><tr><th>ID пользователя</th><th>Имя пользователя</th><th>Колличество сообщений</th></tr>')
	if groupData.postsStatistic.posterTop is not None:
		if len(groupData.postsStatistic.posterTop) > 0:
			pr = groupData.postsStatistic.posterTop[0]
			minMessageCount = (pr.userProvideredMessageCount/2) - 1
	for poster in groupData.postsStatistic.posterTop:
		if poster.userProvideredMessageCount < minMessageCount:
			break
		print ('<tr>')
		print ('<td>'+ linkToHost(poster.userID) +'</td>')
		print ('<td>'+ linkToVK(poster.userID,poster.userName) +'</td>')
		print ('<td>'+ str(poster.userProvideredMessageCount) +'</td>')
		print ('</tr>')
	print ('</table>')
	print ('</td>')
	print ('</tr>')
	print ('</table>')
else:
	print ('</table>')
print ("</body></html>")



