#!/usr/bin/env python3.5 -B


import vk
import datetime
import time
from datetime import date
from time import mktime
import operator
from vk.exceptions import VkAuthError, VkAPIError

def vkAPI():
	return vk.API(vk.Session(),v='5.52')

maxCountPerRquest = 10

def getComments(owner_id, post_id):
	try:
		groupComments = vkAPI().wall.getComments(owner_id=owner_id ,post_id = post_id ,count=maxCountPerRquest, offset = 0,  sort = "asc", extended = 1)
	except VkAPIError as e:
		print("Load post comment error :"+str(e))
		return {}
	else:
		pass
	finally:
		pass
	return groupComments

def getReposts(owner_id, post_id):
	try:
		groupReposts = vkAPI().wall.getReposts(owner_id=owner_id ,post_id = post_id ,count=maxCountPerRquest, offset = 0)
	except VkAPIError as e:
		print("Load post reposts error :"+str(e))
		return {}
	else:
		pass
	finally:
		pass
	return groupReposts

def getLikes(owner_id, post_id):
	try:
		groupReposts = vkAPI().likes.getList(type="post",owner_id=owner_id ,item_id = post_id ,filter="likes",extended=1,count=maxCountPerRquest,skip_own=1)
	except VkAPIError as e:
		print("Load post reposts error :"+str(e))
		return {}
	else:
		pass
	finally:
		pass
	return groupReposts
