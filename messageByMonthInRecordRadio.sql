with group_ids as (  
  select idgroup as id_group from public.vkgroup where idgroup = 1959  
)

select 	count(idmessage) as countMessages,
	to_char(to_timestamp(max(createdate)), 'MM-YYYY') as messageDate,
	extract(MONTH FROM to_timestamp(createdate)) as messageMonth, 
	extract(YEAR FROM to_timestamp(createdate)) as messageYear
from public.vkmessage,group_ids 
where idgroup = id_group AND extract(YEAR FROM to_timestamp(createdate)) = extract(YEAR FROM clock_timestamp())
 group by messageMonth,messageYear 
 order by messageYear DESC,messageMonth DESC 