 with group_ids as (  
  select idgroup as id_group from public.vkgroup where idgroup = 1959  
)

select (sum(countMessages) /
(extract(DAY FROM (date_trunc('MONTH', to_timestamp(max(maxDate))) + INTERVAL '1 MONTH - 1 day')::DATE))) as countMessages,
       to_char(to_timestamp(maxDate), 'MM-YYYY') as messageDate
from (
	  select DISTINCT ON (text) count(idmessage) as countMessages,
			createdate as maxDate
		from public.vkmessage,group_ids 
		where 	idgroup = id_group 
				AND extract(YEAR FROM to_timestamp(createdate)) = extract(YEAR FROM clock_timestamp())
		group by text,createdate
) as uniqueMessages
group by messageDate 
order by messageDate DESC  
