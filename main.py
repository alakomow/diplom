import vk
import postgresql
import time

from vk.exceptions import VkAuthError, VkAPIError

def dbApiUrl():
	# For local testing with server data base
	# return 'pq://diplom:diplom20152016@192.168.1.1:5432/diplom'
	# For server side runing
	return 'pq://diplom:diplom20152016@localhost:5432/diplom'

def dbAPI():
	db = postgresql.open(dbApiUrl())
	# get_table = db.prepare("select * from information_schema.tables where table_name = $1")
	# for x in get_table("tables"):
	# 	print(x)
	# print(get_table.first("tables"))
	# x = db.prepare("select * from vkuser")
	# print (x())
	return db;

def createUser(idUser,fName,lName):
	db = dbAPI()
	createRequest = db.prepare("INSERT INTO public.vkuser VALUES ($1,$2,$3)")
	print ("create user for ID ="+str(idUser))
	createRequest(idUser,fName,lName)

def createUserhistory(idUser,idGroup,introductionDate, releaseDate):
	db = dbAPI()
	queryForAllStoredUsers = 'select iduser,idrow from public.vkusersgrouphistory'
	request = db.prepare(queryForAllStoredUsers)
	nextID = len(request()) + 1
	createRequest = db.prepare('INSERT INTO public.vkusersgrouphistory (idrow,iduser,idgroup,introductiondate,releasedate) VALUES ($1,$2,$3,$4,$5)')
	print ("create user history for user ID ="+str(idUser))
	createRequest(nextID,idUser,idGroup,introductionDate, releaseDate)

def updateReleaseDateInUserHistory(idRow, releaseDate):
	db = dbAPI()
	updateRequest = db.prepare('UPDATE public.vkusersgrouphistory SET releasedate=$1 WHERE idrow=$2')
	print ("update release date  for user history ID ="+str(idRow))
	updateRequest(releaseDate, idRow)

def createNewUsersIfNeed(users, group_id):
	db = dbAPI()
	stringIds = ''#.join(str(userID)+',' for userID in users)
	for user in users:
		userId = user.get('uid')
		if userId is not None:
			stringIds += str(userId)+','
	stringIds = stringIds[:-1]
	query = 'select iduser from public.vkuser where iduser IN ('+stringIds+")"
	request = db.prepare(query)
	usersInDB = request();

	if len(usersInDB) > 0:
		for userID in usersInDB:
			for user in users:
				if userID[0] == user.get('uid'):
					users.remove(user)
# add users to data base
	print ('Add user to data base = '+str(len(users)))
	for user in users:
		userUID = user.get('uid')
		createUser(idUser = user.get('uid'), fName = user.get('first_name'), lName = user.get('last_name'))
		createUserhistory(idUser = userUID, idGroup = group_id, introductionDate = time.time(),releaseDate = -1)
	# for memberUser in usersInGroup:
		# updateReleaseDateInUserHistory(idRow = memberUser[1],releaseDate = time.time())


def vkAPI():
	return vk.API(vk.Session())

def buildUsersListForGroup(group_id):
	stepCount = 1000
	lastOffset = 0
	count = 1
	vkapi = vkAPI()
	while lastOffset < count:
		print ("Request users for group ="+str(group_id)+" countFor request="+str(stepCount)+" offset="+str(lastOffset)+" all users count = "+str(count))
		responce = vkapi.groups.getMembers(group_id=group_id,count=stepCount,sort='id_asc',offset = lastOffset,fields='lastName,firstName')
		users = responce.get('users')
		count = responce.get('count')
		stateString = "State "+str(lastOffset/stepCount)+"/"+str(count/stepCount)
		lastOffset = lastOffset + stepCount
		createNewUsersIfNeed(users = users, group_id = group_id)

def loadInfoGroup(group_id):
	vk = vkAPI() 
	responce = vk.groups.getById(group_id = group_id)
	return responce[0]

def createGroup(group_id,group_name,group_domain,lastUpdate):
	db = dbAPI()
	createRequest = db.prepare("INSERT INTO public.vkgroup VALUES ($1,$2,$3,$4)")
	print ("create group for ID ="+str(group_id))
	createRequest(group_id,"'"+str(group_name)+"'", "'"+str(group_domain)+"'", lastUpdate)

def updateGroup(group_id,group_name,group_domain,lastUpdate):
	db = dbAPI()
	updateRequest = db.prepare('UPDATE public.vkgroup SET idgroup=$1, groupname=$2,groupdomain=$3,lastupdate=$4 WHERE idgroup=$1')
	print ("update group for ID ="+str(group_id))
	updateRequest(group_id,"'"+str(group_name)+"'", "'"+str(group_domain)+"'", lastUpdate)

def updateGroupData(groupData):
	db = dbAPI()
	idGroup = groupData.get('gid')
	print ("Id group = "+str(idGroup))
	if idGroup is not None:
		findQuery = 'select * from public.vkgroup where idgroup='+"'"+str(idGroup)+"'"
		dbRequest = db.prepare(findQuery)
		dbResponce = dbRequest()
		if len(dbResponce) == 0:
			createGroup(idGroup,groupData.get('name'),groupData.get('screen_name'),time.time())
		if len(dbResponce) > 0:
			updateGroup(idGroup,groupData.get('name'),groupData.get('screen_name'),time.time())
		return idGroup
	else:
		print ('Id group not found')

def createPost(idPost, postDate,postText,commentsCount,likesCount,repostsCount, idUser, idGroup):
	db = dbAPI()
	createRequest = db.prepare("INSERT INTO public.vkmessage VALUES ($1,$2,$3,$4,$5,$6,$7,$8)")
	print ("create post for ID ="+str(idPost))
	createRequest(idPost,postDate,commentsCount,likesCount,repostsCount,idUser, idGroup,postText)
def updatePost(idPost, postDate,postText,commentsCount,likesCount,repostsCount, idUser, idGroup):
	db = dbAPI()
	updateRequest = db.prepare('UPDATE public.vkmessage SET idmessage=$1, createdate=$2,commentscnt=$3,likescnt=$4,repostscnt=$5, iduser=$6, idgroup=$7, text=$8 WHERE idmessage=$1')
	print ("update post for ID ="+str(idPost))
	# updateRequest(idPost, postDate, commentsCount, likesCount, repostsCount, idUser, idGroup, postText)

def updatePostInDB(postDict, groupid):
	postID = postDict.get('id')
	if postID is not None:
		db = dbAPI()
		findQuery = 'select * from public.vkmessage where idmessage='+str(postID)
		dbRequest = db.prepare(findQuery)
		dbResponce = dbRequest()
		if len(dbResponce) == 0:
			commentsCount = postDict.get('comments');
			if commentsCount is not None:
				commentsCount = commentsCount.get('count');

			likesCount = postDict.get('likes')
			if likesCount is not None:
				likesCount = likesCount.get('count')

			repostsCount = postDict.get('reposts')
			if repostsCount is not None:
				repostsCount = repostsCount.get('count')

			createPost(idPost = postID, postDate = postDict.get('date'),postText = postDict.get('text'), commentsCount = commentsCount,likesCount = likesCount,repostsCount = repostsCount,idUser = postDict.get('from_id'), idGroup = groupid)
		else:
			commentsCount = postDict.get('comments');
			if commentsCount is not None:
				commentsCount = commentsCount.get('count');

			likesCount = postDict.get('likes')
			if likesCount is not None:
				likesCount = likesCount.get('count')

			repostsCount = postDict.get('reposts')
			if repostsCount is not None:
				repostsCount = repostsCount.get('count')

			updatePost(idPost = postID, postDate = postDict.get('date'),postText = postDict.get('text'), commentsCount = commentsCount,likesCount = likesCount,repostsCount = repostsCount,idUser = postDict.get('from_id'), idGroup = groupid)

	return 1

def buildPosts(group_id):
	stepCount = 100
	lastOffset = 0
	count = 1
	vkapi = vkAPI()
	while lastOffset < count:
		print ("Request group posts for group ="+str(group_id)+" countFor request="+str(stepCount)+" offset="+str(lastOffset)+" all users count = "+str(count))
		try:
			responce = vkapi.wall.get(owner_id=-group_id,count=stepCount,offset = lastOffset)

		except VkAPIError as e:
			print("Load posts error :"+str(e))
			return

		else:
			pass
		finally:
			pass
		
		count = responce[0]
		lastOffset = lastOffset + stepCount
		for i in range(1,len(responce)):
			if updatePostInDB(responce[i], group_id) < 1:
				lastOffset = count + 1
				break


groupDict = loadInfoGroup('record')
idGroup = updateGroupData(groupDict)
buildUsersListForGroup(group_id=idGroup)
buildPosts(group_id = idGroup)

	
# us = usersListForGroup(group_id='habr')
# print (us)
