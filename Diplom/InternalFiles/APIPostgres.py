import postgresql

def dbApiUrl():
	# For local testing with server data base
	return 'pq://diplom:diplom20152016@192.168.1.1:5432/diplom'
	# For server side runing
	# return 'pq://diplom:diplom20152016@localhost:5432/diplom'

def _db():
	db = postgresql.open(dbApiUrl())
	return db;
def _insertRequest(table, columnCount):
    insertStr = "INSERT INTO "+table+" VALUES ("
    for i in range(1,columnCount+1):
        insertStr = insertStr+'$'+str(i)+','
    insertStr = insertStr[:-1]
    insertStr = insertStr + ')'
    createRequest = _db().prepare(insertStr)
    return  createRequest

def _updateRequest(table, columnKeys,where):
    updateStr = 'UPDATE '+table+' SET '
    i = 0
    for key in columnKeys:
        i += 1
        updateStr = updateStr+key+'=$'+str(i)+','
    updateStr = updateStr[:-1]
    updateStr = updateStr+' WHERE '+where
    updateRequest = _db().prepare(updateStr)
    return updateRequest

def updateOrCreateGroup(idGroup,grName,grDomain):
    if idGroup is not None:
        findQuery = 'select * from public.vkgroup where idgroup='+"'"+str(idGroup)+"'"
        dbRequest = _db().prepare(findQuery)
        dbResponce = dbRequest()
        request = None
        if len(dbResponce) == 0:
            print('Create group: '+grName)
            request = _insertRequest(table="public.vkgroup",columnCount=3)
        else:
            print('Update group: '+grName)
            request = _updateRequest(table="public.vkgroup",columnKeys=['idgroup','groupname','groupdomain'],where='idgroup='+str
            (idGroup))
        request(idGroup,grName,grDomain)
    else:
        print ('Id group not found')

def updateOrCreateMessage(idGroup,createDate,commentsCnt,likeCnt,repostsCnt,idUser,text,isOriginal):
    if ((idGroup is not None) and (idUser is not  None) and (createDate > 0)):
        findQuery = 'select * from public.vkmessage where idgroup='+"'"+str(idGroup)+"' AND iduser='"+str(idUser)+"' AND createdate='"+str(createDate)+"'"
        dbRequest = _db().prepare(findQuery)
        dbResponce = dbRequest()
        request = None
        if len(dbResponce) == 0:
            print('Create post')
            request = _insertRequest(table="public.vkmessage",columnCount=8)
        else:
            print('Update post')
            request = _updateRequest(table="public.vkmessage",columnKeys=["createdate","commentscnt","likescnt","repostscnt","iduser","idgroup","text","original"],where=" idgroup='"+str(idGroup)+"' AND iduser='"+str(idUser)+"' AND createdate='"+str(createDate)+"'")

        request(createDate,commentsCnt,likeCnt,repostsCnt,idUser,idGroup,text,isOriginal)
    else:
        print ('Error: Synchronization message error.')

def updateOrCreateUser(idUser,fName,lName):
    if idUser is None:
        print('User id not found')
        return
    findQuery = 'select * from public.vkuser where iduser='+"'"+str(idUser)+"'"
    dbRequest = _db().prepare(findQuery)
    dbResponce = dbRequest()
    request = None
    if len(dbResponce) == 0:
        print('Create user: '+fName+" "+lName)
        request = _insertRequest(table="public.vkuser",columnCount=3)
    else:
        print('Update user: '+fName+" "+lName)
        request = _updateRequest(table="public.vkuser",columnKeys=['iduser','firstname','lastname'],where='iduser='+str
        (idUser))
    request(idUser,fName,lName)