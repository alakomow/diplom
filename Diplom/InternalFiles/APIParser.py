import InternalFiles.APIPostgres
import datetime

def dateToStringWithOutTime(date):
    return datetime.datetime.fromtimestamp(
        int(str(date))
    ).strftime('%Y-%m-%d')

def parseGroupDataResponce(responce):
    ID = responce.get('id')
    if ID is None:
        print('Error: ID froup is bad')
        exit(0)
    InternalFiles.APIPostgres.updateOrCreateGroup(idGroup=ID,grName=responce.get('name'),grDomain=responce.get('screen_name'))
    return ID

def parsePostsDataResponce(items,idGroup):
    if items is None:
        return 0
    for item in items:
        text = item.get('text')
        date = item.get('date')

        commentsCount = item.get('comments');
        if commentsCount is not None:
            commentsCount = commentsCount.get('count');

        likesCount = item.get('likes')
        if likesCount is not None:
            likesCount = likesCount.get('count')

        repostsCount = item.get('reposts')
        if repostsCount is not None:
            repostsCount = repostsCount.get('count')

        responce = InternalFiles.APILoader.loadCopyForPost(text='"'+str(text)+'"',cnt=200,start_time=0,end_time=date)
        isOrigin = 1
        if responce is None:
            print('Duplicates for post not found')
            isOrigin = 1
        else:
            items = responce.get('items')
            isDuplicate = False
            for item in items:
                if item.get('date') < date:
                    isDuplicate = True
                    break

            if isDuplicate:
                print('This post is duplicate')
                isOrigin = 0
            else:
                print('This post original')
                isOrigin = 1
        InternalFiles.APIPostgres.updateOrCreateMessage(idGroup=idGroup,createDate=date,commentsCnt=commentsCount,likeCnt=likesCount,repostsCnt=repostsCount,idUser=item.get('from_id'),text = text,isOriginal=isOrigin)

    return len(items)

def parseUsersDataResponce(responce,idGroup):
    if responce is None:
        print('Responce empty')
        return 0
    items = responce.get('items')
    if items is None:
        print('Responce empty')
        return 0
    for item in items:
        InternalFiles.APIPostgres.updateOrCreateUser(idUser=item.get('id'),fName=item.get('first_name'),lName=item.get('last_name'))
    return len(items)