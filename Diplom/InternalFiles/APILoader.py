import vk
from vk.exceptions import VkAPIError

def _API():
    vkApiParams = {'v':5.42}
    vkApi = vk.API(vk.Session(),60,**vkApiParams)
    return vkApi

def loadGroupInfo(id_group):
     print('Loading '+id_group+' Data...')

     try:
       resp =   _API().groups.getById(group_id = id_group)
       return resp[0]
     except VkAPIError as error:
         print('Load group data error: '+str(error))
         pass
     finally:
         pass

def loadGroupPosts(id_group,cnt,offsetCnt):
    print('Load posts for group ID='+str(id_group)+' for offset='+str(offsetCnt)+' and try load max count='+str(cnt))

    try:
        resp = _API().wall.get(owner_id=-id_group,count=cnt,offset = offsetCnt)
        return resp;
    except VkAPIError as error:
        print('Load group data error: '+str(error))
        pass

    finally:
         pass

def loadCopyForPost(text,cnt,start_time,end_time):

    try:
        resp = _API().newsfeed.search(q=text,count=cnt,start_time=start_time, end_time=end_time)
        return resp
    except VkAPIError as error:
        print('Load group data error: '+str(error))
        pass

    finally:
         pass

def loadGroupMembers(id_group,cntPerRequest,offsetCnt):
    print('Load users for group = '+str(id_group))

    try:
        resp = _API().groups.getMembers(group_id=id_group,count=cntPerRequest,sort='id_asc',offset = offsetCnt,fields='lastName,firstName')
        return resp
    except VkAPIError as error:
        print('Load group data error: '+str(error))
        pass

    finally:
         pass