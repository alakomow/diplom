import sys
import datetime

import InternalFiles.APILoader
import InternalFiles.APIPostgres
import InternalFiles.APIParser

def printHelp():
    print('Man.')
    print('Script params:')
    print('     --help (-h) - For print help message')
    print('     '+id_group_key+' - For passing id group or short name of group (Requared)')

def toDateString(date):
    return datetime.datetime.fromtimestamp(
        int(str(date))
    ).strftime('%Y-%m-%d %H:%M:%S')

def dateToStringWithOutTime(date):
    return datetime.datetime.fromtimestamp(
        int(str(date))
    ).strftime('%Y-%m-%d')

id_group_key = "--group_id"
help_key = "--help"
help_short_key = "-h"
undefinedParamKey = "undefinedParamKey"

argKeys = [id_group_key,help_key,help_short_key]

isNeedPrintHelp = True
argDict = {}
argKey = "file_path"
buffValue = "Undefined"
parsingArgState = 0 #0 - skip 1 - save arg to argKey 2 - save data in to dictionary
for eachArg in sys.argv:
    if parsingArgState == 1:
        if argKeys.count(eachArg) >=1:
            argKey = eachArg
            argDict[argKey] = buffValue
            parsingArgState = 2
        else:
            argDict[undefinedParamKey] = eachArg
    else:
        argDict[argKey] = eachArg
        parsingArgState = 1

if not (argDict.get(undefinedParamKey) is None):
    print('Error: Bad params, please check call script string')
    printHelp()
    exit(0)
if argDict.get(id_group_key) is None:
    print('Error: ID group not found!')
    printHelp()
    exit(0)
if not (argDict.get(help_key)  is None) or not (argDict.get(help_short_key) is None):
    printHelp()
    exit(0)

# load group data
responce = InternalFiles.APILoader.loadGroupInfo(argDict.get(id_group_key))

if responce is None:
    responce = "None"
    print('Error: Group responce Data = '+str(responce))
    exit(0)
# parsing group date
ID = InternalFiles.APIParser.parseGroupDataResponce(responce)
#load posts
loadedPosts = 1
offsetCnt = 0
while loadedPosts > 0:
    responce = InternalFiles.APILoader.loadGroupPosts(id_group=ID,cnt=100,offsetCnt=offsetCnt)
    if responce is None:
        print('WARNING: for group='+str(ID)+' posts not found')
        loadedPosts = 0;
    else:
        # parsing posts
       loadedPosts = InternalFiles.APIParser.parsePostsDataResponce(items=responce.get('items'),idGroup=ID)
       offsetCnt += loadedPosts

# load users
loadedUsers = 1
offsetCnt = 0;
while loadedUsers > 0:
    responce = InternalFiles.APILoader.loadGroupMembers(id_group=ID,cntPerRequest=1000,offsetCnt=offsetCnt)
    if responce is None:
        print('Statisctic for group = '+str(ID)+' not found')
        loadedUsers = 0
    else:
     loadedUsers = InternalFiles.APIParser.parseUsersDataResponce(responce=responce,idGroup=ID)
     offsetCnt += loadedUsers


